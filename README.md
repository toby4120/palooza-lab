<img src="https://pbs.twimg.com/profile_images/966926652310634498/FEhUYEMH.jpg" />

# [PALOOZA LAB](http://pixgame.doc.pixnet/) &middot; [![Build Status](https://img.shields.io/circleci/project/github/RedSparr0w/node-csgo-parser.svg)](http://pixgame.doc.pixnet/) [![Rating](https://img.shields.io/github/package-json/dependency-version/zeit/next.js/dev/@babel/preset-react.svg)](http://pixgame.doc.pixnet/) [![Rating](https://img.shields.io/amo/stars/dustman.svg)](http://pixgame.doc.pixnet/)

> 主要為 Palooza 練習用 REPO

## Getting Started
> 以下為此專案主要相關內容

- [Installation](#installation)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Build](#build)
- [Deploy](#deploy)
- [Tests](#tests)
- [Team](#team)
- [FAQ](#faq)
- [Support](#support)
- [License](#license)

---


## Team

| <a href="#" target="_blank">**Frontend**</a> |
| :---: |
| [![Toby](https://avataaars.io/?avatarStyle=Circle&topType=WinterHat2&accessoriesType=Kurt&hatColor=PastelOrange&hairColor=Blonde&facialHairType=BeardLight&facialHairColor=Platinum&clotheType=ShirtVNeck&clotheColor=Blue02&eyeType=Default&eyebrowType=Default&mouthType=Default&skinColor=Light&s=200)](https://gitlab.com/toby4120/palooza-lab) |
| <a href="https://github.com/tobychung" target="_blank">`TOBY`</a> |



## License

[![License](https://img.shields.io/apm/l/vim-mode.svg)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2019 © <a href="#" target="_blank">Palooza</a>.
